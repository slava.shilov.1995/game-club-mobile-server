"""GameClubMobile URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from GCMApp.views import send_games, send_tournaments, add_user, send_registration_mail, send_members
from rest_framework.routers import DefaultRouter

app_name = "GCMApp"
router = DefaultRouter()
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^$', views.redirect_to_welcome, name='redirect_to_welcome'),
    path('api/send-games/', send_games),
    path('api/send-tournaments/', send_tournaments),
    path('api/send-members/', send_members),
    path('api/add-user/', add_user),
    path('api/send-registration-mail/', send_registration_mail)
]