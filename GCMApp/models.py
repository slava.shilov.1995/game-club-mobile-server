from django.db.models import *


class Game(Model):
    name = CharField(max_length=80, blank=False)

    def __str__(self):
        return self.name


class Tournament(Model):
    game = ForeignKey(Game, on_delete=CASCADE, blank=False)
    title = CharField(max_length=80, blank=False)
    date = DateTimeField(auto_now=False, blank=False)
    active = BooleanField(default=False, blank=False)
    description = TextField(max_length=1024, blank=True)
    winner = CharField(max_length=80, blank=True)

    def __str__(self):
        return self.title


class Member(Model):
    tournament = ForeignKey(Tournament, on_delete=CASCADE, blank=False)
    firstname = CharField(max_length=80, blank=False)
    surname = CharField(max_length=80, blank=False)
    nickname = CharField(max_length=80, blank=False)
    email = EmailField(max_length=80, blank=False)

    def __str__(self):
        return self.nickname


