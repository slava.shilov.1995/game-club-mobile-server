from django.shortcuts import render
from django.utils.html import escape

from django.http import HttpResponse, JsonResponse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions

from rest_framework.decorators import api_view

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from . import serializers
from .models import *
from django.core.mail import send_mail
from django.conf import settings

@api_view(['GET'])
def send_games(request):
    return Response({
        'games': serializers.GameSerializer(Game.objects.all(), many=True).data,
    })

@api_view(['GET'])
def send_tournaments(request):
    return Response({
        'tournaments': serializers.TournamentSerializer(Tournament.objects.all().order_by('date'), many=True).data,
    })

@api_view(['GET'])
def send_members(request):
    return Response({
        'members': serializers.MemberSerializer(Member.objects.all(), many=True).data,
    })

@api_view(['POST'])
def add_user(request):
    print(request.data)
    if Tournament.objects.filter(pk=request.data['tournament']).exists():
        if Tournament.objects.get(pk=request.data['tournament']).active:
            if Member.objects.filter(tournament=request.data['tournament']).filter(email=request.data['email']).exists():
                return Response(status=status.HTTP_200_OK, data={"message": "error: member with that email already registrated"})
        else:
            return Response(status=status.HTTP_200_OK, data={"message": "error: tournament is inactive"})
    else:
        return Response(status=status.HTTP_200_OK, data={"message": "error: tournament not found"})

    tournament = Tournament.objects.get(pk=request.data['tournament'])
    member = Member(
        firstname=request.data['firstname'],
        surname=request.data['surname'],
        email=request.data['email'],
        tournament=tournament,
        nickname=request.data['nickname']
    )
    member.save()

    subject = 'Регистрация на турнир'
    text = request.data['nickname'] + '! Вы зарегистрированы на турнир ' + tournament.title + '! Молите о пощаде!'
    to_email = [request.data['email'], ]
    send_mail(
        subject,
        text,
        settings.DEFAULT_FROM_EMAIL,
        to_email,
        fail_silently=False,
    )

    return Response(status=status.HTTP_200_OK, data={"message": "sent"})


@api_view(['POST'])
def send_registration_mail(request):
    send_mail(
        'Test',
        'Hello',
        'gameclubmobilenn@mail.ru',
        ['slava.shilov.1995@mail.ru'],
        fail_silently=False,
    )

    return Response(status=status.HTTP_200_OK, data={"message": "mail sent"})


def welcome(request):


    # docs = db.collection(u'games').get()
    # games = {}
    # for doc in docs:
    #     name = doc.to_dict()['name']
    #     games[name] = {'name':name}
    #     # text += game.to_dict()['name']
    #     print(u'{} -> {}'.format(doc.id, doc.to_dict()))
    #
    # context = {"games":games}



    context = {}
    context['games'] = Game.objects.all()

    return render(request, 'welcome.html', context)

def redirect_to_welcome(request):

    return HttpResponseRedirect("Welcome/")