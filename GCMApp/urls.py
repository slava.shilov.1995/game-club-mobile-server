from django.urls import path
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
urlpatterns = [
    path('send-games/', views.send_games),
]