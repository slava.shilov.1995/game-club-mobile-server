from django.apps import AppConfig


class GcmappConfig(AppConfig):
    name = 'GCMApp'
