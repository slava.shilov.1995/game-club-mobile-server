from django.contrib import admin

from .models import Game, Tournament, Member

admin.site.register(Game)

@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ('firstname', 'surname', 'nickname')
    list_filter = ('tournament',)

@admin.register(Tournament)
class TournamentAdmin(admin.ModelAdmin):
    list_display = ('game', 'title')
    list_filter = ('game',)